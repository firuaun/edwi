#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function
from tiktok import Tik as tik
from logger import Logger as log
import pierwsze
import sys, re, urlparse
import socket 

def lastoccur(s,c):
    return len(s)-s[::-1].find(c)-1

def joinaddr(addr,path):
    parsedurl = pierwsze.parseUrl(addr)
    if path.startswith('/'):
        return "%s://%s%s" % (parsedurl['protocol'],parsedurl['host'],path)
    elif parsedurl['path'] and parsedurl['path'].find('/') != -1:
        return "%s://%s/%s%s" % (parsedurl['protocol'],
                                parsedurl['host'],
                                parsedurl['path'][:lastoccur(parsedurl['path'],'/')+1],
                                path)
    else:
        return "%s://%s/%s" % (parsedurl['protocol'],parsedurl['host'],path)

def getAddrByUrl(url):
    parsed = pierwsze.parseUrl(url)
    log.debug("getAddrByUrl: %s" % parsed)
    return socket.gethostbyname(parsed['host'].strip())

class AnchorAnalizer(object):

    def __init__(self,addr,depth=5):
        self._localurl = addr
        self._visited = []
        self._rdepth = depth
        self._localaddr = getAddrByUrl(self._localurl)

    def _getAnchores(self, rurl, content):
        anchors = filter(lambda x: "mailto:" not in x, set(re.findall(r'\<a.+?href=[\"\'](.+?)[\"\']',content,flags=re.DOTALL)))
        for i,a in enumerate(anchors):
            anchors[i] = urlparse.urljoin(rurl,a.strip())
        return anchors

    def _islocal(self,addr):
        return getAddrByUrl(addr) == self._localaddr

    def _isvisited(self,addr):
        return addr in self._visited

    def _check(self,addr):
        return True

    def _recursive_analisis(self,addr,n):
        if n < self._rdepth:
            if not self._isvisited(addr) and self._check(addr):
                anchors = []
                rurl = None
                try:
                    rurl, c = pierwsze.getWebSrc(addr)
                    anchors = self._getAnchores(rurl,c)
                    if self._islocal(addr):
                        print(addr,file=self._flocal)
                    else:
                        print(addr,file=self._fextern)
                except Exception as e:
                    log.warn("Warning during fetching the %s [%s] (%s)."%(addr,e,rurl))
                    pass
                self._visited.append(addr)
                for a in anchors:
                    self._recursive_analisis(a,n+1)


    def analize(self,flname='local_links.txt',fename='extern_links.txt'):
        self._flocal = open(flname,'w+')
        self._fextern = open(fename,'w+')
        self._recursive_analisis(self._localurl,0)
        self._flocal.close()
        self._fextern.close()


if __name__ == "__main__":
    log.setLevel(log.DEBUG)
    log.info("Start link analisis...")
    aa = AnchorAnalizer(sys.argv[1] if len(sys.argv) > 1 else raw_input("Provide url: "),4)
    aa.analize()
    log.info("End link analisis")
