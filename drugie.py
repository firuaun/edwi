#!/usr/bin/python
# -*- coding: utf-8 -*-
import re, sys
import pierwsze
from tiktok import Tik as tik
from logger import Logger as l

class Count(dict):
	def __missing__(self,key):
		return 0

def getWordsCount(txt):
	m = Count()
	for word in txt.split(' '):
		m[word] += 1
	return m

def getTopWords(txt, k=None, threshold=None):
	l.setLevel(l.DEBUG)
	run_time = tik()

	l.info("Start counting the words...")
	m = getWordsCount(txt)

	sm = sorted(m, key=m.get, reverse=True)
	sm = sm[:k] if k != None else sm
	sm = filter(lambda x: m.get(x) > threshold, sm) if threshold != None else sm

	l.info("Word counting run time %d ms." % run_time.tok())
	l.info("Top word list (TRESH=%s, k=%s):" % (threshold,k))
	for i,x in enumerate(sm,start=1):
		l.info("%d. [%d] %s" % (i,m[x],x))

	l.info("Total counting run time %d ms." % run_time.tok())

if __name__ == "__main__":
	_, txt = pierwsze.getParsedWeb()
	getTopWords(txt,10,4)
