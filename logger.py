#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function

class Logger(object):

	NONE = 0
	ERR = 1
	WARN = 2
	INFO = 3
	DEBUG = 4

	level = ERR

	@staticmethod
	def err(msg, nl=True):
		msg = "[ERROR] " + msg
		Logger.log(msg,nl,Logger.ERR)

	@staticmethod
	def warn(msg, nl=True):
		msg = "[WARN] " + msg
		Logger.log(msg,nl,Logger.WARN)

	@staticmethod
	def info(msg, nl=True):
		msg = "[INFO] " + msg
		Logger.log(msg,nl,Logger.INFO)

	@staticmethod
	def debug(msg, nl=True):
		msg = "[DEBUG] " + msg
		Logger.log(msg,nl,Logger.DEBUG)

	@staticmethod
	def log(*args):
		msg = args[0].decode('utf-8','remove').encode('utf8')
		if args[2] <= Logger.level:
			if args[1]:
				print(args[0])
			else:
				print(args[0],end='')

	@staticmethod
	def setLevel(lvl):
		Logger.level = lvl