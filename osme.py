#!/usr/bin/python
# -*- coding: utf-8 -*-
import lucene, sys
from java.io import File, StringReader
from org.apache.lucene.document import Document, Field, StringField, TextField
from szoste import queryUsage, color, Lucene
from czwarte import AnchorAnalizer
import pierwsze, drugie
from math import sqrt
import robotparser, sqlite3, pickle
from os import path, remove
from logger import Logger as log
import urllib, urllib2,json,re
from siodme import PageIndexer, WebAnalizer
import signal

class WebpageFilters(object):
	class Filter(object):
		def __init__(self,addr,regex_filter):
			self.a = re.compile(addr,re.DOTALL|re.IGNORECASE)
			self.f = re.compile(regex_filter,re.DOTALL|re.IGNORECASE|re.MULTILINE)
		def isfor(self,addr):
			return self.a.search(addr) is not None

		def filter(self,content):
			res = self.f.findall(content)
			return res if res is not None else []

	FILTERS = [
		Filter('reddit.com\/.+?\/comments\/','data-type="comment".+?\<a.+?class.+?author.+?\>(.+?)<\/a>.+?\<div.+?class.+?usertext-body.+?\>(.+?)\<\/div\>'),
		Filter('reddit.com\/.+?\/comments\/','entry.+?\<a.+?class.+?author.+?\>(?P<author>.+?)<\/a>.+?\<div.+?class.+?usertext-body.+?\>(?P<text>.+?)\<\/div\>'),
		Filter('reddit.com\/user\/','entry.+?\<a.+?class.+?author.+?\>(?P<author>.+?)<\/a>.+?\<div.+?class.+?usertext-body.+?\>(?P<text>.+?)\<\/div\>')
	]

	@staticmethod
	def get(address):
		r = []
		for f in WebpageFilters.FILTERS:
			if f.isfor(address):
				r.append(f)
		return r

class PostsIndexer(PageIndexer):
	def __init__(self,index='postsindex',filters=WebpageFilters):
		super(PostsIndexer,self).__init__(index)
		self.filters = filters
		self.hashCache = []

	def addPosts(self,url,content):
		filters = self.filters.get(url)
		writer = self.luc.getWriter()
		for filtr in filters:
			for post in filtr.filter(content):
				h = hash(post)
				if h not in self.hashCache:
					self.hashCache.append(h)
					parsed_author = self.parseBody(post[0])
					parsed_content = self.parseBody(post[1])
					doc = Document()
					doc.add(Field("address", url, TextField.TYPE_STORED))
					doc.add(Field("author", parsed_author, TextField.TYPE_STORED))
					doc.add(Field("content", parsed_content, TextField.TYPE_STORED))
					doc.add(Field("sentiment", pickle.dumps(PostsIndexer.getSentiment(parsed_content)),StringField.TYPE_STORED))
					writer.addDocument(doc)

	@staticmethod
	def getSentiment(content):
		return SentimentAnalizer.check(content).data

	def sigintHandler(self,signum,frame):
		log.debug("Sigint handler... Commiting...")
		self.luc.getWriter().commit()
		sys.exit(0)


class SentimentAnalizer:
	URL_PATH = 'http://text-processing.com/api/sentiment/'
	class Sample(object):
		def __init__(self,payload="{}"):
			self.data = json.loads(payload)
		def getOverallScore(self):
			return self.data['label']
		def getPosScore(self):
			return self.data['probability']['pos']
		def getNeutralScore(self):
			return self.data['probability']['neutral']
		def getNegScore(self):
			return self.data['probability']['neg']
	@staticmethod
	def check(text):
		log.debug('[SentimentAnalizer::check] text: %s...' % text[:15])
		data = urllib.urlencode({'text':text})
		request = urllib2.urlopen(urllib2.Request(SentimentAnalizer.URL_PATH),data)
		return SentimentAnalizer.Sample(request.read())

def usage():
    print color.BOLD+color.RED+"Usage:"+color.END+""" 
        """+color.BOLD+"Parse:"+color.END+"""
        \tpython osme.py {p|parse} <index dir> <start page> <depth>
        """+color.BOLD+"Search:"+color.END+"""
        \tpython osme.py {s|search} <index dir> <query>
        """+color.BOLD+"Check sentiment:"+color.END+"""
        \tpython osme.py {c|check} <word|"sentence with spaces"|url>
        """+color.BOLD+"Help:"+color.END+"""
        \tpython osme.py {h|help}"""

class CacheWords(object):

	CACHED_WORDS_FILE = 'cached.words'

	def __init__(self):
		self.modified = False
		self.cache = CacheWords.getCache()

	@staticmethod
	def getCache():
		try:
			with open(CacheWords.CACHED_WORDS_FILE,'r+') as f:
				return pickle.loads(f.read())
		except:
			return {}

	def get(self,word):
		data = self.cache.get(word,None)
		try:
			if data is None:
				data = SentimentAnalizer.check(word)
				self.cache[word] = data.data
				self.modified = True
				return data
			else:
				sample = SentimentAnalizer.Sample()
				sample.data = data
				return sample
		except:
			log.err("Cached word error durring fetch...")
			return None
	
	def save(self):
		if self.modified:
			log.debug("Saving cached words...")
			with open(CacheWords.CACHED_WORDS_FILE,'w+') as f:
				f.write(pickle.dumps(self.cache))

if __name__ == '__main__':
	log.setLevel(log.DEBUG)
	MAX_TOP_DOCKS = 5
	if len(sys.argv) < 2:
		log.warn("Too little args.")
		usage()
		queryUsage()
	else:
		pi = PostsIndexer(sys.argv[2]) if len(sys.argv) > 2 else PostsIndexer()
		if sys.argv[1] in ('p','parse'):
			start_page = sys.argv[3] if len(sys.argv) > 3 else 'http://wykop.pl'
			depth = int(sys.argv[4]) if len(sys.argv) > 4 else 2
			log.info("SP: %s\nDEPTH: %d"%(start_page,depth))
			w = WebAnalizer(start_page,pi.addPosts,depth)
			ww = pi.luc.getWriter()
			try:
				signal.signal(signal.SIGINT, pi.sigintHandler)
				w.analize()
			finally:
				log.debug('Commiting...')
				ww.commit()
				ww.close()
		elif sys.argv[1] in ('s', 'search'):
			searcher = pi.luc.getSearcher()
			log.debug("Got %d pages in %s" % (pi.luc.getReader().numDocs(),pi.luc.index_dir))
			print("Possible fields:\naddress\tauthor\tcontent")
			query_phrase = sys.argv[5] if len(sys.argv) > 5 else raw_input("Query: ")
			query = pi.luc.query("content", query_phrase)
			MAX = 1000
			hits = searcher.search(query, MAX)
			hl = pi.luc.getHighlighter(query)
			log.info("Found %d document(s) that matched query '%s'. Only top %d are displayed." % (hits.totalHits, query, MAX_TOP_DOCKS))
			log.info("Results:")
			for hit in hits.scoreDocs[:MAX_TOP_DOCKS]:
				print "---------------"
				print "Stats: ",hit.score, hit.doc, hit.toString()
				doc = searcher.doc(hit.doc)
				txt = doc.get("content")
				txtlen = len(txt)
				hl.setMaxDocCharsToAnalyze(txtlen)
				ts = pi.luc.analyzer.tokenStream("content", StringReader(txt))
				print "Address: %s" % (doc.get("address"),)
				print "Author: %s" % (doc.get("author"),)
				best = hl.getBestFragments(ts, txt, 4, "...")
				print "Content[%d]: %s" % (txtlen,best if len(best) > 0 else txt)
				print "Sentiment:"
				sentiment = pickle.loads(doc.get("sentiment"))
				print "\t[%s] [+]: %f\t [|]: %f\t [-]: %f" % (sentiment['label'],sentiment['probability']['pos'],sentiment['probability']['neutral'],sentiment['probability']['neg'])
		elif sys.argv[1] in ('c', 'check'):
			text = sys.argv[2] if len(sys.argv) > 2 else raw_input('Query: ')
			if "http" in text:
				cached_words = CacheWords()
				try:
					log.debug('Detected url. Fetch and parse operations pending...')
					words = drugie.getWordsCount(pierwsze.parseWeb(pierwsze.getWebSrc(text)[1].lower())) #urllib.urlopen(text).read().lower()
					log.debug('Starting sentiment analyze for each word...')
					stat_words = {key:cached_words.get(key) for key in words.keys() if len(key) > 2 and not key.isdigit()}
					print "The top 5 most positive words:"
					for i,pos in enumerate(sorted(stat_words,key=lambda x: stat_words[x].data['probability']['pos'] if stat_words[x] is not None else 0,reverse=True)[:5],start=1):
						print "%d. %s - pos: %f" % (i,pos,stat_words[pos].data['probability']['pos'])
					print "The top 5 most negative words:"
					for i,neg in enumerate(sorted(stat_words,key=lambda x: stat_words[x].data['probability']['neg'] if stat_words[x] is not None else 0,reverse=True)[:5],start=1):
						print "%d. %s - neg: %f" % (i,neg,stat_words[neg].data['probability']['neg'])
				except Exception as e:
					log.err(str(e))
				finally:
					cached_words.save()
			else:
				sentiment = SentimentAnalizer.check(text)
				print "Sentiment analize of '%s':\nOverall: %s\n\tPositive: %f\n\tNeutral: %f\n\tNegative: %f" % (text,sentiment.getOverallScore(),sentiment.getPosScore(), sentiment.getNeutralScore(),sentiment.getNegScore())
		elif sys.argv[1] in ('h', 'help'):
			usage()
			queryUsage()
		else:
			log.warn("Unknown command.")
			usage()
			queryUsage()
