#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, re, sys, lucene
from subprocess import *
from logger import Logger as log

from java.io import File
from org.apache.lucene.analysis.miscellaneous import LimitTokenCountAnalyzer
from org.apache.lucene.analysis.standard import StandardAnalyzer
from org.apache.lucene.index import IndexWriter, IndexWriterConfig, IndexReader
from org.apache.lucene.search import IndexSearcher
from org.apache.lucene.queryparser.classic import QueryParser
from org.apache.lucene.document import Document, Field, StringField, TextField
from org.apache.lucene.store import SimpleFSDirectory
from org.apache.lucene.util import Version


#http://graus.co/blog/pylucene-4-0-in-60-seconds-tutorial/

def indexDirectory(dir,w):

    for name in os.listdir(dir):
        path = os.path.join(dir, name)
        if os.path.isfile(path):
            indexFile(dir, name, w)
        elif os.path.isdir(path):
        	indexDirectory(path,w)


def indexFile(dir, filename, writer):

    path = os.path.join(dir, filename)
    print "  File: ", filename

    if filename.endswith('.zip'):
    	print 'COMMAND: gunzip -c -q ' + path
        child = Popen('gunzip -c -q ' + path, shell=True, stdout=PIPE, cwd=os.path.dirname(dir)).stdout
    else:
        child = Popen('cat ' + path + ' | col -bx',
                      shell=True, stdout=PIPE, cwd=os.path.dirname(dir)).stdout

    data = child.read()
    err = child.close()
    if err:
        raise RuntimeError, '%s failed with exit code %d' %(command, err)

    matches = re.search('^Title: (.+?)[\r\n]', data,
                        re.MULTILINE | re.DOTALL)

    title = matches and matches.group(1) or ''

    matches = re.search('^Author: (.+?)[\r\n]', data,
                        re.MULTILINE | re.DOTALL)
    author = matches and matches.group(1) or ''

    matches = re.search('^Translator: (.+?)[\r\n]', data,
                        re.MULTILINE | re.DOTALL)
    translator = matches and matches.group(1) or ''

    matches = re.search('^Release Date: (.+?)[\r\n\[]', data,
                        re.MULTILINE | re.DOTALL)
    rdate = matches and matches.group(1) or ''

    matches = re.search('^Language: (.+?)[\r\n]', data,
                        re.MULTILINE | re.DOTALL)
    language = matches and matches.group(1) or ''

    keywords = ' '.join((title, author, translator, language))

    doc = Document()
    doc.add(Field("title", title.strip(), StringField.TYPE_STORED))
    doc.add(Field("author", author.strip(), StringField.TYPE_STORED))
    doc.add(Field("translator", translator.strip(), TextField.TYPE_STORED))
    doc.add(Field("release date", rdate.strip(), TextField.TYPE_STORED))
    doc.add(Field("language", language.strip(), TextField.TYPE_STORED))
    doc.add(Field("keywords", keywords,
                  TextField.TYPE_NOT_STORED))
    doc.add(Field("filename", os.path.abspath(path), StringField.TYPE_STORED))

    print "title: ", title
    print "author: ", author
    print "translator: ", translator
    print "language: ", language
    print "keywords: ", keywords

    writer.addDocument(doc)

def usage():
    print """Usage: 
        \tParse:
        \t\tpython piate.py {p|parse} <index dir> <parse dir>
        \tSearch:
        \t\tpython piate.py {s|search} <index dir> <field> <query>"""

class Lucene(object):
    def __init__(self,index_dir="index",limit=10000):
        self.index_dir = index_dir
        lucene.initVM(vmargs=['-Djava.awt.headless=true'])
        self.directory = SimpleFSDirectory(File(index_dir))
        self.analyzer = StandardAnalyzer(Version.LUCENE_CURRENT)
        self.writer = None
        self.reader = None

    def getWriter(self):
        self.writer = self.writer if self.writer else IndexWriter(self.directory, IndexWriterConfig(Version.LUCENE_CURRENT, self.analyzer))
        return self.writer

    def getReader(self):
        self.reader = self.reader if self.reader else IndexReader.open(self.directory)
        return self.reader

    def getSearcher(self):
        return IndexSearcher(self.getReader())

    def query(self,field,qp):
        return QueryParser(Version.LUCENE_CURRENT, field, self.analyzer).parse(qp)

if __name__ == '__main__':
    log.setLevel(log.DEBUG)
    if len(sys.argv) < 2:
        log.warn("Too little args.")
        usage()
    else:
        luc = Lucene(sys.argv[2]) if len(sys.argv) > 2 else Lucene()
        if sys.argv[1] in ('p','parse'):
            w = luc.getWriter()
            dir = os.path.join(os.getcwd(), sys.argv[3] if len(sys.argv) > 3 else 'gut/data/')
            for name in os.listdir(dir):
                path = os.path.join(dir, name)
                if os.path.isdir(path):
                    indexDirectory(path,w)
            w.commit()
            w.close()
        elif sys.argv[1] in ('s', 'search'):
            searcher = luc.getSearcher()
            log.debug("Got %d docs in %s" % (luc.getReader().numDocs(),luc.index_dir))
            field = sys.argv[3] if len(sys.argv) > 3 else raw_input("Field to search: ")
            query_phrase = sys.argv[4] if len(sys.argv) > 4 else raw_input("Query: ")
            query = luc.query(field, query_phrase)
            MAX = 1000
            hits = searcher.search(query, MAX)

            log.info("Found %d document(s) that matched query '%s'." % (hits.totalHits, query))
            log.info("Results:")
            for hit in hits.scoreDocs:
                print "---------------"
                print "Stats: ",hit.score, hit.doc, hit.toString()
                doc = searcher.doc(hit.doc)
                print "Title: %s\nAuthor: %s\nLang: %s\nTranslator: %s\nPath: %s\n" % (doc.get("title"),doc.get("author"),doc.get("language"),doc.get("translator"),doc.get("filename"))
        else:
            log.warn("Unknown command.")
            usage()