#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function
import urllib2 as url
import time
import re
import chardet
import sys
from logger import Logger as log


def determineCharset(req,rawdata):
	ctype = req.headers["content-type"]
	log.debug("content-type %s" % ctype)
	rencode = re.search('charset=(?P<encode>.+?)(\s|$)',ctype)
	if rencode != None:
		return rencode.group('encode')
	else:
		rencode = re.search('charset=(?P<encode>.+?)\"',rawdata)
		if rencode != None:
			return rencode.group('encode')
		else:
			return chardet.detect(rawdata).get('encoding','utf8')


def millisec():
	return int(round(time.time()*1000))

def getWebSrc(addr):
	web = url.urlopen(addr,timeout=5)
	content = web.read()
	encoding = determineCharset(web,content)
	log.debug("encoding %s [%s]" % (encoding,addr))
	content = content.decode(encoding if encoding is not None else 'utf8','ignore')
	real_url = web.geturl()
	web.close()
	return (real_url,content)

def saveToFile(content, fname):
	f = file(fname, 'w')
	f.write(content.encode('utf-8'))
	f.close()

def parseUrl(url):
	return re.search("^(?P<protocol>.+?):\/\/(?P<host>.+?)(\/(?P<path>.+?)(\?(?P<params>.*)|$)|\/$|$)",url).groupdict()

def getFileNameFromWebAddr(addr):
	daddr = parseUrl(addr)
	host = daddr.get("host")
	path = daddr.get("path")
	if not path or path == '':
		path = 'index.html'
	else:
		path = re.sub('\.(php|html|htm)','',path) + '.html'
	return re.sub("[\/]",'_',("[%s] %s" % (host.strip(), path.strip())))


def getAddr():
	if len(sys.argv) > 1:
		return sys.argv[1]
	else:
		return raw_input("Put address: ")

def parseWeb(content):
	rules = (
		(r'(<script.*?>.*?</script>|<style.*?>.*?</style>)',' '),
		(r'(<.*?>|&.+?;)', ' '),
		(ur'[^A-Za-z0-9ąćęńśłóżźĄĆĘŚŁŃÓŻŹ]',' '),
		(r'\s{2,}',' ')
	)
	for rule in rules:
	    content = re.sub(rule[0],rule[1],content,flags=re.DOTALL)

	return content


def getParsedWeb(adr=None):
	log.setLevel(log.DEBUG)
	address = adr if adr else getAddr()

	total_run_start_time = millisec()
	log.info("Start fetching the web...")

	rurl, content = getWebSrc(address)
	parse_start_time = millisec()
	web_fetching_time = parse_start_time-total_run_start_time
	log.info("Parse address...")

	fname = getFileNameFromWebAddr(rurl)
	log.info("Save to file...")

	saveToFile(content, fname)

	log.info("Start parsing the file...")

	content = parseWeb(content)
	log.info("Save parsed content to file...")

	content = content.lower().strip()
	saveToFile(content, "%s.txt" % fname)
	end_time = millisec()
	log.info("Fetch web time %d ms." % web_fetching_time)
	log.info("Parse run time %d ms." % (end_time - parse_start_time))
	log.info("Total run time %d ms." % (end_time - total_run_start_time))
	return rurl,content

if __name__ == "__main__":
	getParsedWeb()
