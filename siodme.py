#!/usr/bin/python
# -*- coding: utf-8 -*-
import lucene, sys
from java.io import File, StringReader
from org.apache.lucene.document import Document, Field, StringField, TextField
from szoste import queryUsage, color, Lucene
from czwarte import AnchorAnalizer
import pierwsze, drugie
from math import sqrt
import robotparser, sqlite3, pickle
from os import path, remove
from logger import Logger as log

class StringUtil(object):
	@staticmethod
	def similarity(o1,o2):
		base = [drugie.getWordsCount(o1),drugie.getWordsCount(o2)]
		words = {word for page in base for word in page.keys()}
		vectors = []
		vectors_mod = []
		word_count = float(len(words))
		for page in base:
			v = []
			s = 0
			for word in words:
				f = page.get(word,0)/word_count
				v.append(f)
				s += f**2
			vectors.append(v)
			vectors_mod.append(sqrt(s))
		s = 0
		for i,_ in enumerate(words):
			s += vectors[0][i]*vectors[1][i]
		return s/(vectors_mod[0]*vectors_mod[1])

class PageIndexDb(object):
	def __init__(self,index='webindex',drop=False):
		self.dbname = "%s.db" % index
		self.db = None
		if drop:
			self.drop()
		self.create()

	def connect(self):
		self.db = self.db if self.db is not None else sqlite3.connect(self.dbname)
		return self.db

	def disconnect(self):
		self.db.close()
		self.db = None

	def create(self):
		if not path.isfile(self.dbname):
			cur = self.connect().cursor()
			cur.execute('''CREATE TABLE similarity (id integer primary key autoincrement, docId integer, data blob)''')
			self.db.commit()
			cur.close()
			return True
		return False

	def insert(self,id,data):
		cur = self.connect().cursor()
		cur.execute('INSERT INTO similarity VALUES (?,?,?)',(None,id,pickle.dumps(data)))
		id = cur.lastrowid
		self.db.commit()
		cur.close()
		return id
		
	def select(self,id):
		cur = self.connect().cursor()
		d = cur.execute('SELECT data FROM similarity WHERE docId=?',(id,)).fetchone()[0]
		cur.close()
		return pickle.loads(d)


	def drop(self):
		if path.isfile(self.dbname):
			remove(self.dbname)
			return True
		return False

class PageIndexer(object):
	def __init__(self,index='webindex'):
		self.luc = Lucene(index)

	def addPage(self,url,content):
		log.info("Add %s to %s." % (url,self.luc.index_dir))
		writer = self.luc.getWriter()
		doc = Document()
		doc.add(Field("address", url, TextField.TYPE_STORED))
		doc.add(Field("content", self.parseBody(content),TextField.TYPE_STORED))
		writer.addDocument(doc)

	def parseBody(self,body):
		return pierwsze.parseWeb(body)

	def getSimilarDocuments(self, index):
		r = self.luc.getReader()
		indexDoc = r.document(index)
		simList = []
		for i in range(r.maxDoc()):
			if i != index:
				comparedDoc = r.document(i)
				sim = StringUtil.similarity(indexDoc.get("content"),comparedDoc.get("content"))
				simList.append([i,sim])
		return sorted(simList,key=lambda x: x[1],reverse=True)

class RobotGuard(object):

	class FakeRobot(object):
		def __init__(self,returnvalue):
			self.r = returnvalue
		def can_fetch(self,ua,path):
			return self.r

	def __init__(self):
		self.map = dict()
	def can(self,url):
		canIt = True
		parsed = None
		robotTxtPath = ""
		try:
			parsed = pierwsze.parseUrl(url)
			robotTxtPath = "%s://%s/robots.txt"%(parsed['protocol'],parsed['host'])
			rp = self.map.get(parsed['host'],None)
			if rp is None:
				rp = robotparser.RobotFileParser(robotTxtPath)
				rp.read()
				self.map[parsed['host']] = rp
			canIt = rp.can_fetch("*",url)
		except Exception as e:
			log.warn("No robot.txt? %s " %(e,))
			if parsed is not None:
				self.map[parsed['host']] = self.FakeRobot(True)
		if not canIt:
			log.debug(color.RED+"Robots"+color.END+" are forbiden on %s" % url)
		return canIt

class WebAnalizer(AnchorAnalizer):
	SKIP_EXT = ('.jpeg','.gif','.png','.jpg','.iso','.gz','.zip','.tar','.rar', '.pdf')

	def __init__(self,lineup,onparse,depth=3):
		self.sup = super(WebAnalizer,self)
		lineup = lineup if isinstance(lineup,(list,tuple)) else [lineup]
		self.sup.__init__(lineup[0],depth)
		self._localurl = lineup
		self.onparse = onparse
		self.robot = RobotGuard()

	def _getAnchores(self, rurl, content):
		self.onparse(rurl,content)
		return self.sup._getAnchores(rurl,content)

	def skip(self,url):
		for ext in WebAnalizer.SKIP_EXT:
			if ext in url:
				return True
		return False

	def _check(self,addr):
		skip = False
		robot = True
		try:
			skip = self.skip(addr)
			robot = self.robot.can(addr)
		except Exception as e:
			log.err("Error occured: %s" % e)
		finally:
			return not skip and robot

	def analize(self,fl='/dev/null',fe='/dev/null'):
		try:
			self._flocal = open(fl,'w+')
			self._fextern = open(fe,'w+')
			for x in self._localurl:
				self._recursive_analisis(x,0)
			self._flocal.close()
			self._fextern.close()
		except Exception as e:
			log.err("An envitable error occured: %s" % e)

def usage():
    print color.BOLD+color.RED+"Usage:"+color.END+""" 
        """+color.BOLD+"Parse:"+color.END+"""
        \tpython siodme.py {p|parse} <index dir> <start page> <depth>
        """+color.BOLD+"Search:"+color.END+"""
        \tpython siodme.py {s|search} <index dir> <query>
        """+color.BOLD+"Help:"+color.END+"""
        \tpython siodme.py {h|help}"""


if __name__ == '__main__':
	log.setLevel(log.DEBUG)
	MAX_TOP_DOCKS = 5
	if len(sys.argv) < 2:
		log.warn("Too little args.")
		usage()
		queryUsage()
	else:
		pi = PageIndexer(sys.argv[2]) if len(sys.argv) > 2 else PageIndexer()
		if sys.argv[1] in ('p','parse'):
			start_page = sys.argv[3] if len(sys.argv) > 3 else 'http://wykop.pl'
			depth = int(sys.argv[4]) if len(sys.argv) > 4 else 2
			log.info("SP: %s\nDEPTH: %d"%(start_page,depth))
			w = WebAnalizer(start_page,pi.addPage,depth)
			ww = pi.luc.getWriter()
			w.analize()
			ww.commit()
			ww.close()
		elif sys.argv[1] in ('s', 'search'):
			searcher = pi.luc.getSearcher()
			log.debug("Got %d pages in %s" % (pi.luc.getReader().numDocs(),pi.luc.index_dir))
			print("Possible fields:\naddress\tcontent")
			query_phrase = sys.argv[5] if len(sys.argv) > 5 else raw_input("Query: ")
			query = pi.luc.query("content", query_phrase)
			MAX = 1000
			hits = searcher.search(query, MAX)
			hl = pi.luc.getHighlighter(query)
			log.info("Found %d document(s) that matched query '%s'. Only top %d are displayed." % (hits.totalHits, query, MAX_TOP_DOCKS))
			log.info("Results:")
			for hit in hits.scoreDocs[:MAX_TOP_DOCKS]:
				print "---------------"
				print "Stats: ",hit.score, hit.doc, hit.toString()
				doc = searcher.doc(hit.doc)
				txt = doc.get("content")
				txtlen = len(txt)
				hl.setMaxDocCharsToAnalyze(txtlen)
				ts = pi.luc.analyzer.tokenStream("content", StringReader(txt))
				print "Address: %s" % (doc.get("address"),)
				print "Content[%d]: %s" % (txtlen,hl.getBestFragments(ts, txt, 4, "..."))
				print "Similar:"
				for i,similar in enumerate(pi.getSimilarDocuments(hit.doc)[:3],start=1):
					print "%d.\t%s\t%f" % (i, searcher.doc(similar[0]).get("address"),similar[1])
		elif sys.argv[1] in ('h', 'help'):
			usage()
			queryUsage()
		else:
			log.warn("Unknown command.")
			usage()
			queryUsage()
