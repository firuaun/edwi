#!/usr/bin/python
# -*- coding: utf-8 -*-
import time

class Tik(object):

	def __init__(self):
		self.start = Tik.millisec()

	def tok(self):
		return Tik.millisec() - self.start

	@staticmethod
	def millisec():
		return int(round(time.time()*1000))