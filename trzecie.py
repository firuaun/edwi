#!/usr/bin/python
# -*- coding: utf-8 -*-
from logger import Logger as log
from tiktok import Tik as tik
import pierwsze as p
import drugie as d
from math import sqrt
import itertools

def getRandomInCategoryWikiUrl(cat):
	return "http://nodyip.url.ph/rwiki/?c=%s" % cat

if __name__ == "__main__":
	log.setLevel(log.DEBUG)
	base = []
	log.info("Start fetching the materials...")
	pages_url_base = []
	for c in ["Computers", "Organ_improvisers", "Physics"]:
		caddr = getRandomInCategoryWikiUrl(c)
		i=0
		while i < 5:
			url, txt = p.getParsedWeb(caddr)
			if url not in pages_url_base:
				pages_url_base.append(url)
				base.append(d.getWordsCount(txt))
				i += 1
	log.info("Done getting word base...")
	words = {word for page in base for word in page.keys()}

	vectors = []
	vectors_mod = []
	word_count = float(len(words))
	log.debug("Word count: %d" % int(word_count))
	for page in base:
		v = []
		s = 0
		for word in words:
			f = page.get(word,0)/word_count
			v.append(f)
			s += f**2
		vectors.append(v)
		vectors_mod.append(sqrt(s))
	log.debug("Module vector [%d]: %s" % (len(vectors_mod),vectors_mod))
	results = []
	for pair in itertools.combinations(range(len(base)),2):
		s = 0
		v1 = vectors[pair[0]]
		v2 = vectors[pair[1]]
		for i,_ in enumerate(words):
			s += v1[i]*v2[i]
		result = s/(vectors_mod[pair[0]]*vectors_mod[pair[1]])
		results.append((pair,result))
	log.debug("Results vector [%d]: %s" % (len(results),results))
	sort_results  = sorted(results,key=lambda x: x[1],reverse=True)
	log.debug("Sorted results [%d]: %s" % (len(sort_results),sort_results))
	for i,p in enumerate(pages_url_base,start=1):
		log.debug("[%d]: %s" % (i,p))
	log.info("Najbardziej podobne [3]:")
	for i,r in enumerate(sort_results[:3],start=1):
		log.info("%d. [%f] [%s] [%s]" % (i,r[1],pages_url_base[r[0][0]],pages_url_base[r[0][1]]))
	log.info("Najmniej podobne [3]:")
	for i,r in enumerate((sort_results[::-1])[:3],start=1):
		log.info("%d. [%f] [%s] [%s]" % (i,r[1],pages_url_base[r[0][0]],pages_url_base[r[0][1]]))


	#need to search in the results the best and worst pairs
